extends RigidBody


# TODO make a global/preload thing to get this or use groups
onready var offset = get_parent().level_size

export(int) var copies_per_side = 1
export(bool) var is_flipped = false

export(Curve) var volume_curve
export(float) var volume_max_speed = 5

export(Curve) var collision_curve
export(float) var collision_min_speed
export(float) var collision_max_speed
export(float) var collision_static_fac = 4

const flip_v: Vector3 = Vector3(1, -1, -1)

var left_copies = []
var right_copies = []

# Called when the node enters the scene tree for the first time.
func _ready():
	
	var children = get_children()
	var visuals_node = Spatial.new()
	for child in children:
		if not child is CollisionShape:
			visuals_node.add_child(child.duplicate())
	
	for _i in range(copies_per_side):
		var left_copy = visuals_node.duplicate()
		left_copies.append(left_copy)
		var right_copy = visuals_node.duplicate()
		right_copies.append(right_copy)
		add_child(left_copy)
		add_child(right_copy)
	
	_update_copy_transforms()
	
	if is_flipped:
		gravity_scale *= -1

func _update_copy_transforms():
	var flipped = false
	for i in range(len(left_copies)):
		flipped = not flipped
		var copy: Spatial = left_copies[i]
		copy.global_transform.basis = global_transform.basis
		copy.global_transform.origin = global_transform.origin - Vector3(offset * (i + 1), 0, 0)
		if flipped:
			copy.global_transform = copy.global_transform.scaled(flip_v)
	
	flipped = false
	for i in range(len(right_copies)):
		flipped = not flipped
		var copy: Spatial = right_copies[i]
		copy.global_transform.basis = global_transform.basis
		copy.global_transform.origin = global_transform.origin + Vector3(offset * (i + 1), 0, 0)
		if flipped:
			copy.global_transform = copy.global_transform.scaled(flip_v)

func _flip():
	gravity_scale *= -1
	linear_velocity *= flip_v
	angular_velocity *= flip_v

func _flip_left():
	_flip()
	global_transform.origin -= Vector3(offset, 0, 0)
	global_transform = global_transform.scaled(flip_v)
	_update_copy_transforms()
	
func _flip_right():
	_flip()
	global_transform.origin += Vector3(offset, 0, 0)
	global_transform = global_transform.scaled(flip_v)
	_update_copy_transforms()

func _physics_process(delta):
	if get_parent().player_node == null:
		return
	
	var x_distance = global_transform.origin.x - get_parent().player_node.global_transform.origin.x
	
	#print(x_distance)
	
	# Move the rigidbody to always be in the same section as the player
	if x_distance > offset / 2.0:
		_flip_left()
	elif x_distance < -offset / 2.0:
		_flip_right()
	_update_sound()

func _update_sound():
	if len(contacts) > 0:
		var speed = clamp(linear_velocity.length() / volume_max_speed, 0, 1)
		$CollisionShape5/SlideSound.unit_db = volume_curve.interpolate_baked(speed)

func _process(delta):
	_update_copy_transforms()


var contacts = []
func _on_Crate_body_entered(body):
	contacts.append(body)
	var relative_speed
	if body is RigidBody:
		relative_speed = (linear_velocity - body.linear_velocity).length()
	else:
		relative_speed = linear_velocity.length()
	
	print(relative_speed)
	if relative_speed > collision_min_speed:
		var speed_fac = clamp(relative_speed / collision_max_speed, 0, 1)
		
		$CollisionShape5/DropSound.unit_db = collision_curve.interpolate_baked(speed_fac)
		$CollisionShape5/DropSound.play()


func _on_Crate_body_exited(body):
	contacts.erase(body)
