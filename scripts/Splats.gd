extends AudioStreamPlayer

export(Array, AudioStream) var sounds

export(float) var min_pitch_scale = 0.8
export(float) var max_pitch_scale = 1.4


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func play_random():
	stream = sounds[randi() % len(sounds)]
	pitch_scale = rand_range(min_pitch_scale, max_pitch_scale)
	play()
