extends Control

# Called when the node enters the scene tree for the first time.
func _ready():
	visible = false

func show_menu(time):
	if GameState.is_last_level():
		$Menu/NextLevel.visible = false
	else:
		$Menu/NextLevel.visible = true
	
	var minutes = floor(time / 60)
	var seconds = floor(time - 60.0 * minutes)
	var fraction = time - 60.0 * minutes - seconds
	fraction = ("%.2f" % fraction).split(".")[1]
	var time_str = "Time: %02d:%02d.%s" % [minutes, seconds, fraction]

	$Menu/Title/HBoxContainer/Time.text = time_str
	$AnimationPlayer.play("show")
	$Menu/NextLevel.grab_focus()
	
	var level = GameState.level
	GameState.player_high_scores[level] = time
	
	$Menu/Leaderboard.load_leaderboard()


func _on_NextLevel_pressed():
	if GameState.is_last_level():
		pass
	else:
		GameState.next_level()


func _on_Replay_pressed():
	GameState.level_lost()


func _on_Exit_pressed():
		GameState.exit_to_menu()


# TODO Submit score
func _on_Button_pressed():
	$SubmitMenu.popup()


func _on_SubmitMenu_submit_pressed(player_name):
	print("Submitting player score for %s" % player_name)
	$Menu/Leaderboard.submit_score(player_name)
	$Menu/Title/HBoxContainer/Button.disabled = true
