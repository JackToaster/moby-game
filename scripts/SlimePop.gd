extends Spatial

export(int) var num_balls = 35
export(float) var max_vel = 15
export(float) var y_vel = 3
export(PackedScene) var slimeball

# Called when the node enters the scene tree for the first time.
func _ready():
	for _i in range(num_balls):
		var ball: RigidBody = slimeball.instance()
		add_child(ball)
		ball.linear_velocity = Vector3(rand_range(-max_vel, max_vel), rand_range(-max_vel, max_vel) + y_vel, rand_range(-max_vel, max_vel))


func _on_Timer_timeout():
	GameState.level_lost()
