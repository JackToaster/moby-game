extends Spatial

export(NodePath) var following_path

export(Vector3) var offset
export(int) var num_copies = 1

onready var following: Spatial = get_node(following_path)
onready var offset_x = offset.x

var copies = []

const flip_v: Vector3 = Vector3(1, -1, -1)

# Called when the node enters the scene tree for the first time.
func _ready():
	# Add all children to a child spatial node
	var children = get_children()
	var current: Spatial = Spatial.new()
	current.transform.origin -= Vector3(offset_x * 2 * floor(num_copies / 2), 0, 0)
	for child in children:
		remove_child(child)
		current.add_child(child)
	add_child(current)
	
	copies.append(current)
	# Repeatedly copy and flip the children
	var inverted = false
	for _i in range(num_copies):
		inverted = not inverted
		
		current = current.duplicate(7)
		add_child(current)
		if inverted:
			current.transform.origin += offset * flip_v
		else:
			current.transform.origin += offset
		current.rotate_x(PI)
		copies.append(current)


var last_half_x_index: int = 0
func _process(_delta):
	if following == null:
		return
	var x_index: float = following.global_transform.origin.x / offset_x
	var half_x_index: int = ceil(x_index / 2)  # The amount of "full loops" to translate by
	
	if half_x_index != last_half_x_index:
		var difference: int = half_x_index - last_half_x_index
		var offset: Vector3 = Vector3(difference * offset_x * 2, 0, 0)
		for copy in copies:
			copy.transform.origin += offset
		last_half_x_index = half_x_index


