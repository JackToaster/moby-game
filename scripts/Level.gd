extends Spatial

export(float) var level_size

export(NodePath) var player_node_path
onready var player_node = get_node(player_node_path)

var level_time = 0

var final_time

func _ready():
	GameState.set_level_transition($PreLevel)
	$PreLevel.hide_transition()
	get_tree().paused = true

func _process(delta):
	level_time += delta


func _on_PreLevel_finished_hiding():
	get_tree().paused = false

func finished():
	final_time = level_time

func show_postlevel():
	get_tree().paused = true
	$PostLevel.show_menu(final_time)
