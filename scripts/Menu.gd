extends Spatial

export(PackedScene) var level_icon

func _ready():
	GameState.set_level_transition($PreLevel)
	for i in range(len(GameState.levels)):
		_add_level_icon(i)
	_fade_in_menu()

func _add_level_icon(index):
	var icon: Control = level_icon.instance()
	var button: Button = icon.get_child(0)
	button.text = str(index + 1)
# warning-ignore:return_value_discarded
	button.connect("pressed", self, "_level_button_pressed", [index])
	$LevelMenu/CenterContainer/GridContainer.add_child(icon)

func _level_button_pressed(index: int):
	GameState.level = index
	GameState.start_game()
	$MenuFade.interpolate_property($Music, "volume_db", null, -80, 0.5)
	$MenuFade.start()

func _on_PlayButton_pressed():
	$MenuTransition.play("ToLevels")


func _fade_in_menu():
	$MenuFade.interpolate_property($MainMenu, "modulate", Color.transparent, Color.white, 1)
	$MenuFade.start()

func _fade_out_menu():
	$MenuFade.interpolate_property($MainMenu, "modulate", Color.white, Color.transparent, 0.5)
	$MenuFade.start()


func _on_BackButton_pressed():
	$MenuTransition.play("ToMain")


func _on_SettingsButton_pressed():
	$MenuTransition.play("ToSettings")


func _on_SettingsBackButton_pressed():
	$MenuTransition.play("FromSettings")
