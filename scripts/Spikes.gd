extends Area

func _on_Spikes_body_entered(body: Node):
	if body.has_method("pop"):
		body.pop()
