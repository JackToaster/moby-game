extends PopupMenu

var player_name = ""

signal submit_pressed
func _on_Submit_pressed():
	player_name = $VBoxContainer/Name.text
	emit_signal("submit_pressed", player_name)
	hide()


func _on_Name_text_changed(new_text):
	_validate_name(new_text)


func _on_SubmitMenu_about_to_show():
	_validate_name($VBoxContainer/Name.text)

func _validate_name(text):
	if len(text) > 0:
		$VBoxContainer/Submit.disabled = false
	else:
		$VBoxContainer/Submit.disabled = true
