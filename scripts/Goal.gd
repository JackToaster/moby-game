extends Spatial


func _on_Area_body_entered(body: Node):
	if body.has_method("reached_goal"):
		body.reached_goal(global_transform)
		$Confetti.emitting = true
