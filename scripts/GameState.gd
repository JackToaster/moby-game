extends Node

var player_high_scores = {}

enum State {
	Menu,
	Playing,
	LoadingLevel,
	LoadingMenu
}

var state = State.Menu

export(PackedScene) var menu
export(Array, PackedScene) var levels

export(int) var level = 0

var level_transition: Control
func set_level_transition(trans):
	level_transition = trans
	level_transition.connect("finished_showing", self, "_on_LevelTransition_finished_showing")
	level_transition.connect("finished_hiding", self, "_on_LevelTransition_finished_hiding")

var level_transition_text = ""

func _set_level_text():
	level_transition_text = "Level " + str(level + 1)

func _set_level_text_to(text: String):
	level_transition_text = text
	
func start_game():
	state = State.LoadingLevel
	_set_level_text()
	level_transition.show_transition()

func level_lost():
	state = State.LoadingLevel
	_set_level_text()
	level_transition.show_transition()

func next_level():
	level += 1
	if level >= len(levels):
		level = 0
		
		state = State.LoadingMenu
		# TODO Winning text???
	else:
		state = State.LoadingLevel
		_set_level_text()
	
	level_transition.show_transition()

func exit_to_menu():
	_set_level_text_to("")
	get_tree().change_scene_to(menu)
	get_tree().paused = false
	level_transition.hide_transition()


func _on_LevelTransition_finished_hiding():
	if state == State.LoadingLevel:
		state = State.Playing
	elif state == State.LoadingMenu:
		state = State.Menu


func _on_LevelTransition_finished_showing():
	if state == State.LoadingLevel:
		get_tree().change_scene_to(levels[level])
		call_deferred("_hide_transition")
	elif state == State.LoadingMenu:
		get_tree().change_scene_to(menu)
		call_deferred("_hide_transition")

func _hide_transition():
	level_transition.hide_transition()

func _input(event):
	if state == State.Playing:
		if event.is_action("ui_cancel"):
			if not $PauseMenu.visible:
				$PauseMenu.popup()

func is_last_level():
	return level == len(levels) - 1
