extends HSlider

export(Curve) var volume_curve

onready var bus_idx = AudioServer.get_bus_index("Master")


func _on_Volume_value_changed(value):
	var volume = volume_curve.interpolate(value / 100.0)
	AudioServer.set_bus_volume_db(bus_idx, volume)

