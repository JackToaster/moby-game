extends MeshInstance

export(Array, Color) var colors

# Called when the node enters the scene tree for the first time.
func _ready():
	var mat: SpatialMaterial = mesh.surface_get_material(1)
	randomize()
	var alpha = mat.albedo_color.a
	mat.albedo_color = colors[randi() % len(colors)]
	mat.albedo_color.a = alpha
