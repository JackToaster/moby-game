extends Control

func _ready():
	_update_text()
	visible = false
	set("z", 9999)

func set_text(text: String):
	$TransitionScreen/Label.text = text

func _update_text():
	set_text(GameState.level_transition_text)

func show_transition():
	_update_text()
	visible = true
	$AnimationPlayer.play("Show")
	
func hide_transition():
	_update_text()
	$AnimationPlayer.play("Hide")

signal finished_showing
signal finished_hiding
func _on_AnimationPlayer_animation_finished(anim_name):
	if anim_name == "Hide":
		emit_signal("finished_hiding")
		visible = false
	else:
		emit_signal("finished_showing")
