extends Control

export(String, MULTILINE) var loading_text
export(String, MULTILINE) var header

export(Array, String) var places

var ldb_name
var level

# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func load_leaderboard(submitted = false):
	level = GameState.level
	ldb_name = "l"+str(level)
	$RichTextLabel.bbcode_text = loading_text
	yield(SilentWolf.Scores.get_high_scores(10, ldb_name), "sw_scores_received")
	var scores: Array = SilentWolf.Scores.scores
	display_leaderboard(scores, submitted)


func submit_score(player_name):
	$RichTextLabel.bbcode_text = loading_text
	yield(SilentWolf.Scores.persist_score(player_name, -GameState.player_high_scores[level] * 100.0, ldb_name), "sw_score_posted")
	load_leaderboard(true)


func display_leaderboard(scores, submitted = false):
	var output_text = ""
	
	output_text += header
	
	var my_score = GameState.player_high_scores[level]
	
	yield(SilentWolf.Scores.get_score_position(-my_score * 100.0, ldb_name), "sw_position_received")
	var my_position = SilentWolf.Scores.position - 1
	
	var table_text = ""
	
	if not submitted:
		table_text += _get_row_text(my_position, "You", my_score)
		table_text += "\n[cell][i]p[/i][/cell][cell][i]i[/i][/cell][cell][i]q[/i][/cell]"
	
	for i in range(len(scores)):
		var score = scores[i]
		var time = score.score / 100.0
		var player_name = score.player_name
		table_text += _get_row_text(i, player_name, time)
	
	output_text += "\n[center][table=3]%s[/table][/center]" % table_text
	
	
	$RichTextLabel.bbcode_text = output_text


func _get_row_text(place: int, name: String, time: float):
	time = abs(time)
	
	var minutes = floor(time / 60)
	var seconds = floor(time - 60.0 * minutes)
	var fraction = time - 60.0 * minutes - seconds
	fraction = ("%.2f" % fraction).split(".")[1]
	var time_str = "%02d:%02d.%s" % [minutes, seconds, fraction]
	
	
	return "\n[cell]%s    [/cell][cell]%s    [/cell][cell]%s[/cell]" % [_get_place_column_text(place), _sanitize(name), time_str]

func _sanitize(uname: String):
	uname = uname.replace("[", "(")
	uname = uname.replace("]", ")")
	return uname

func _get_place_column_text(place: int):
	if place < len(places):
		return places[place]
	else:
		# Stupid off by one errors
		place += 1
		var number = "[b]%d[/b]" % place
		if place % 10 == 1:
			return number + "st"
		elif place % 10 == 2:
			return number + "nd"
		elif place % 10 == 3:
			return number + "rd"
		else:
			return number + "th"
