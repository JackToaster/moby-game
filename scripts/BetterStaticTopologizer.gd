extends Spatial

# TODO make a global/preload thing to get this or use groups
var offset
export(float) var offet_override = 0

export(int) var copies_per_side = 2


const flip_v: Vector3 = Vector3(1, -1, -1)

# Called when the node enters the scene tree for the first time.
func _ready():
	if offet_override != 0:
		offset = offet_override
	else:
		offset = get_parent().level_size
	# Add all children to a child spatial node
	var children = get_children()
	var center: Spatial = Spatial.new()
	
	for child in children:
		remove_child(child)
		center.add_child(child)
	add_child(center)
	
	# Right side
	var current = center
	for _i in range(copies_per_side):
		current = current.duplicate(7)
		add_child(current)
		current.transform.origin += Vector3(offset, 0, 0)
		current.rotate_x(PI)
	
	# Left side
	current = center
	for _i in range(copies_per_side):
		current = current.duplicate(7)
		add_child(current)
		current.transform.origin -= Vector3(offset, 0, 0)
		current.rotate_x(PI)


func _flip_left():
	var z_pos = global_transform.origin.z
	global_transform.origin -= Vector3(offset, 0, 0)
	global_transform = global_transform.scaled(flip_v)
	global_transform.origin.z = z_pos
	
func _flip_right():
	var z_pos = global_transform.origin.z
	global_transform.origin += Vector3(offset, 0, 0)
	global_transform = global_transform.scaled(flip_v)
	global_transform.origin.z = z_pos

func _process(_delta):
	if get_parent().player_node == null:
		return
	
	var x_distance = global_transform.origin.x - get_parent().player_node.global_transform.origin.x
	
	# Move the rigidbody to always be in the same section as the player
	if x_distance > offset / 2.0:
		_flip_left()
	elif x_distance < -offset / 2.0:
		_flip_right()
	
