extends Node


# Called when the node enters the scene tree for the first time.
func _ready():
	pass


func _on_Timer_timeout():
	yield(SilentWolf.Scores.get_high_scores(), "sw_scores_received")
	print("Scores: " + str(SilentWolf.Scores.scores))

func _on_Timer2_timeout():
	SilentWolf.Scores.persist_score("test", -10, "l0")
	SilentWolf.Scores.persist_score("test2", -11, "l2")
	SilentWolf.Scores.persist_score("test3", -12.6, "l1")
	SilentWolf.Scores.persist_score("test4", -9.99, "l3")
	SilentWolf.Scores.persist_score("test6", -10, "l4")

