extends RigidBody

export(float) var jump_vertical = 10
export(float) var jump_normal = 5
export(float) var jump_gravity = 0.7

export(float) var jump_leeway = 0.1 # Seconds after leaving platform that you can jump
export(float) var jump_timeout = 0.15 # Seconds after jumping that you cant jump

export(float) var move_speed = 3
export(float) var move_force = 10
export(float) var air_move_force = 3

export(PackedScene) var slime_pop

export(Curve) var sliding_volume_curve
export(Curve) var sliding_pitch_curve


var colliding_shapes = []

var can_jump = false

onready var floor_shape_ids = [0, 2, 3]

onready var anim_state_machine = $slime/AnimationTree["parameters/playback"]
onready var anim_tree = $slime/AnimationTree


enum AnimState {
	Moving,
	Falling,
	Jumping,
	Winning
}
var anim_state = AnimState.Moving

# Called when the node enters the scene tree for the first time.
func _ready():
	$JumpTimer.wait_time = jump_leeway
	$JumpTimeout.wait_time = jump_timeout

func _set_anim_state(new_state):
	anim_state = new_state
	if new_state == AnimState.Moving:
		anim_state_machine.travel("Moving")
	elif new_state == AnimState.Falling:
		anim_state_machine.travel("Falling")
	elif new_state == AnimState.Jumping:
		anim_state_machine.travel("Jump")
	elif new_state == AnimState.Winning:
		anim_state_machine.travel("slurp")
		$WinTimer.start()
		axis_lock_linear_x = true
		axis_lock_linear_y = true

func _physics_process(_delta):
	if anim_state == AnimState.Winning:
		return
	
	if _check_floor_collisions():
		$"trail particles".emitting = true
		can_jump = true
		$JumpTimer.stop()
	else:
		$"trail particles".emitting = false
	
	if Input.is_action_just_pressed("jump"):
		_jump()
	
	if Input.is_action_pressed("jump"):
		gravity_scale = jump_gravity
	else:
		gravity_scale = 1.0
	
	_slide_around()
	
	_update_sound()
	
	#print(can_jump)

func _process(_delta):
	if can_jump:
		if anim_state == AnimState.Falling:
			_set_anim_state(AnimState.Moving)
	else:
		if $JumpTimeout.time_left == 0 and anim_state == AnimState.Moving:
			_set_anim_state(AnimState.Falling)
	
	if anim_state == AnimState.Falling:
		anim_tree["parameters/Falling/Blend/blend_amount"] = clamp(linear_velocity.x / move_speed, -1, 1)
	elif anim_state == AnimState.Moving:
		anim_tree["parameters/Moving/Blend/blend_amount"] = clamp(linear_velocity.x / move_speed, -1, 1)

func _check_floor_collisions():
	for shape_id in floor_shape_ids:
		if shape_id in colliding_shapes:
			return true
	return false


func _jump():
	if can_jump and $JumpTimeout.time_left == 0:
		_apply_jump_impulse()
		$JumpTimeout.start()
		_set_anim_state(AnimState.Jumping)

func _apply_jump_impulse():
	apply_central_impulse(Vector3(0, jump_vertical, 0))

func _slide_around():
	var side_input = 0
	if Input.is_action_pressed("left"):
		side_input -= 1.0
	if Input.is_action_pressed("right"):
		side_input += 1.0
	
	var velocity_relative_input = side_input - ((linear_velocity.x - get_floor_velocity_x()) / move_speed)
	
	var move_force_vec = Vector3(velocity_relative_input, 0, 0)
	if _check_floor_collisions():
		move_force_vec *= move_force
	else:
		move_force_vec *= air_move_force
	
	add_central_force(move_force_vec)

func _update_sound():
	if can_jump:
		var speed_fac = abs((linear_velocity.x - get_floor_velocity_x()) / move_speed)
		var volume = sliding_volume_curve.interpolate_baked(speed_fac)
		var pitch = sliding_pitch_curve.interpolate_baked(speed_fac)
		
		$AudioStreamPlayer.volume_db = volume
		$AudioStreamPlayer.pitch_scale = pitch
	else:
		$AudioStreamPlayer.volume_db = -80
	

func _add_colliding_shape(local_shape):
	# Just hitting a floor/wall
	if local_shape in floor_shape_ids and not can_jump and not _check_floor_collisions():
		$"splat particles".emitting = true
		$SquashAnimation.stop()
		$SquashAnimation.play("squash")
		$Splats.play_random()
	colliding_shapes.append(local_shape)

func _remove_colliding_shape(local_shape):
	colliding_shapes.erase(local_shape)
	if not _check_floor_collisions():
		$JumpTimer.start()

func _on_Player_body_shape_entered(_body_id, _body, _body_shape, local_shape):
	_add_colliding_shape(local_shape)

func _on_Player_body_shape_exited(_body_id, _body, _body_shape, local_shape):
	_remove_colliding_shape(local_shape)


func _on_JumpTimer_timeout():
	can_jump = false


func _on_JumpTimeout_timeout():
	if anim_state == AnimState.Jumping:
		anim_state = AnimState.Falling

func _input(event):
	if event.is_action_pressed("restart"):
		pop()

func pop():
	var s_pop = slime_pop.instance()
	get_parent().add_child(s_pop)
	s_pop.global_transform = global_transform
	queue_free()

func reached_goal(goal_pos: Transform):
	if goal_pos.basis.y.y < 0:
		$Tween.interpolate_method(self, "_set_rot", 0, PI, 0.25)
		$Tween.start()
	$Tween.interpolate_property(self, "global_transform:origin", global_transform.origin, goal_pos.origin - Vector3(0, 0.5, 0) * goal_pos.basis.y, 0.5)
	$Tween.start()
	$Slurp.play()
	var level = get_parent()
	if level.has_method("finished"):
		level.finished()
	else:
		print("Error: Can't finish level because not child of level node!")
	_set_anim_state(AnimState.Winning)

func _set_rot(new_rot: float):
	$slime.rotation.z = new_rot

var floors = []

func _on_Player_body_entered(body: Node):
	if body.is_in_group("MovingFloor"):
		floors.append(body)


func _on_Player_body_exited(body):
	if body.is_in_group("MovingFloor") and body in floors:
		floors.erase(body)

func get_floor_velocity_x():
	if len(floors) > 0:
		var first_floor: StaticBody = floors[0]
		return first_floor.constant_linear_velocity.x
	else:
		return 0


func _on_WinTimer_timeout():
	var level = get_parent()
	if level.has_method("show_postlevel"):
		level.show_postlevel()
	else:
		print("Error: Can't finish level because not child of level node!")
