extends "res://scripts/TopologicalStaticBody.gd"

enum CloudState {
	Stopped,
	MovingL,
	MovingR
}

export(CloudState) var cloud_state = CloudState.Stopped
export(float) var cloud_speed = 1

func _ready():
	_update_cloud_state(cloud_state)

func _update_cloud_state(new_state):
	if new_state == CloudState.Stopped:
		constant_linear_velocity = Vector3.ZERO
		cloud_state = new_state
	elif new_state == CloudState.MovingL:
		if $DebounceTimer.time_left == 0:
			constant_linear_velocity = global_transform.basis.z * cloud_speed
			cloud_state = new_state
	elif new_state == CloudState.MovingR:
		if $DebounceTimer.time_left == 0:
			constant_linear_velocity = -global_transform.basis.z * cloud_speed
			cloud_state = new_state
	
	$DebounceTimer.start()

func _physics_process(delta):
	global_transform.origin += constant_linear_velocity * delta

var left_bodies = []
var right_bodies = []

func _on_LeftPush_body_entered(body):
	if body == self:
		return
	left_bodies.append(body)
	_pushed()
func _on_LeftPush_body_exited(body):
	while body in left_bodies:
		left_bodies.erase(body)

func _on_RightPush_body_entered(body):
	if body == self:
		return
	right_bodies.append(body)
	_pushed()
func _on_RightPush_body_exited(body):
	while body in right_bodies:
		right_bodies.erase(body)

func _pushed():
	
	# Pushed while stationary
	if cloud_state == CloudState.Stopped:
		# Pushed from the left
		if len(left_bodies) > 0:
			if len(right_bodies) == 0:
				_update_cloud_state(CloudState.MovingR)
		
		# Pushed from the right
		elif len(right_bodies) > 0:
			if len(left_bodies) == 0:
				_update_cloud_state(CloudState.MovingL)
	
	# Pushed while moving
	elif cloud_state == CloudState.MovingL:
		if len(left_bodies) > 0:
			_update_cloud_state(CloudState.Stopped)
	elif cloud_state == CloudState.MovingR:
		if len(right_bodies) > 0:
			_update_cloud_state(CloudState.Stopped)
	
