extends PopupMenu


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.



func _on_PauseMenu_about_to_show():
	get_tree().paused = true



func _on_ExitButton_pressed():
	hide()
	GameState.exit_to_menu()


func _on_ResumeButton_pressed():
	hide()


func _on_PauseMenu_popup_hide():
	get_tree().paused = false
